#!/bin/bash
python scripts/baseline.py --dataset dstc2_data --dataroot data --trackfile focus.json --focus True
python scripts/score.py --dataset dstc2_data --dataroot data --trackfile focus.json --ontology scripts/config/ontology_dstc2.json --scorefile focus.score.csv
python scripts/report.py --scorefile focus.score.csv
