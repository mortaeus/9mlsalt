labels = {'nu = 0.001'; 'nu = 0.005'; 'nu = 0.01'; 'nu = 0.05'; 'nu = 0.1'};
colorstring = 'kbgry';

test_success = [35.5  31.5  38.0  38.0  40.0  47.5  39.0  48.0  47.5  46.0;
                21.0  29.5  31.5  36.0  29.5  30.5  36.0  32.0  33.0  31.5;
                49.5  49.0  51.0  53.0  32.5  29.0  55.0  47.5  56.5  52.5;
                36.0  33.5  35.0  37.5  32.0  29.5  35.5  33.0  35.5  11.0;
                36.0  30.5  37.0  31.5  33.0  28.5  35.0  32.0  35.0  30.5];

train_success = [33.5  45.0  39.5  36.5  45.5  49.5  49.5  45.5  42.0  54.0;
                38.0  46.0  50.5  43.5  40.5  41.0  35.5  29.0  34.0  36.0;
                38.0  54.5  59.5  55.0  45.5  34.0  38.0  40.5  49.5  46.0;
                39.0  50.5  47.5  45.0  39.0  38.5  28.0  30.5  38.5  32.0;
                42.5  56.5  47.0  40.5  40.5  37.5  32.0  32.0  32.5  35.5];

%plot test
x = 1:10;
plots = zeros(1, 5);
for i = 1:5
    plots(i) = plot(x, test_success(i, :), 'DisplayName', labels(i), 'Color', colorstring(i));
    hold on;
end

legend(plots, 'Location', 'south');

print('test_results.pdf', '-dpdf');

input('wait');

%plot test
x = 1:10;
plots = zeros(1, 5);
for i = 1:5
    plots(i) = plot(x, train_success(i, :), 'DisplayName', labels(i), 'Color', colorstring(i));
    hold on;
end

legend(plots, 'Location', 'south');

print('train_results.pdf', '-dpdf');

input('wait');

