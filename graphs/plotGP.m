labels = {'linear'; 'gaussian'};
colorstring = 'kb';

test_success_lin_gauss = [44.0, 54.5, 59.5, 65.5, 63.5, 67.0, 65.5, 73.5, 70.0, 75.0;
                          59.5, 66.5, 64.0, 71.0, 70.5, 71.5, 68.5, 63.5, 71.5, 67.5];

train_success_lin = [49.0, 57.0, 64.5, 65.0, 73.0, 69.0, 68.0, 73.0, 70.5, 73.0];

%plot test
x = 1:10;
plots = zeros(1, 2);
for i = 1:5
    plots(i) = plot(x, test_success(i, :), 'DisplayName', labels(i), 'Color', colorstring(i));
    hold on;
end

legend(plots, 'Location', 'south');

print('test_results.pdf', '-dpdf');

input('wait');

%plot train
x = 1:10;
plots = zeros(1, 1);
for i = 1:5
    plots(i) = plot(x, train_success(i, :), 'DisplayName', labels(i), 'Color', colorstring(i));
    hold on;
end

legend(plots, 'Location', 'south');

print('train_results.pdf', '-dpdf');

input('wait');

