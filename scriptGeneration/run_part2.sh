#!/bin/bash

function wait_qstat {
    LENGTH=`qstat | wc | awk '{print $1}'`
    while [ $LENGTH -ne 0 ];
    do
        echo "Waiting for qstat"
        sleep 1
        LENGTH=`qstat | wc | awk '{print $1}'`
    done
}

#train
python script_train.py list_2.train mcc
wait_qstat

#test
python script_test.py list_2.test mcc
wait_qstat

./parse_part2.sh
