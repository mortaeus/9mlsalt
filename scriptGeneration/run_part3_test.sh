function wait_qstat {
    LENGTH=`qstat | wc | awk '{print $1}'`
    while [ $LENGTH -ne 0 ];
    do
        echo "Waiting for qstat"
        sleep 1
        LENGTH=`qstat | wc | awk '{print $1}'`
    done
}

function wait_qstat {
    LENGTH=`qstat | wc | awk '{print $1}'`
    while [ $LENGTH -ne 0 ];
    do
        echo "Waiting for qstat"
        sleep 1
        LENGTH=`qstat | grep poly | wc | awk '{print $1}'`
    done
}

wait_qstat

#python ./script_test.py list.poly gp


vals=('0.1' '1.0' '10')
for v1 in "${vals[@]}"
do
    for v2 in "${vals[@]}"
    do
        echo "$v1 $v2" > /home/tfwn2/9MLSALT/cued-python_practical/thetaFile/gaussKernel.${v1}.${v2}
        python ./script_test.py list.gp.${v1}.${v2} gp $v1 $v2
    done
done
