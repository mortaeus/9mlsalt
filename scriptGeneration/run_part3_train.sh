python ./script_train.py list.poly gp

vals=('0.1' '1.0' '10')
for v1 in "${vals[@]}"
do
    for v2 in "${vals[@]}"
    do
        echo "$v1 $v2" > /home/tfwn2/9MLSALT/cued-python_practical/thetaFile/gaussKernel.${v1}.${v2}
        python ./script_train.py list.gp.${v1}.${v2} gp $v1 $v2
    done
done
