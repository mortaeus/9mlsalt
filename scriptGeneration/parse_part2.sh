#!/bin/bash

#get test success mean
OUT_NAME=part2_part2.res
echo "episodes1,episodes2,episodes3,episodes4,episodes5,episodes6,episodes7,episodes8,episodes9,episodes10" > mean_$OUT_NAME
python parseResults.py gRun tra_1_ .log 5 TEST | grep -P 'SUCCESS\tavg' | sed 's/^SUCCESS\tavg\t//g' | sed 's/ \t/,/g' | sed 's/,$//g' >> mean_$OUT_NAME

#get test success ssd
echo "episodes1,episodes2,episodes3,episodes4,episodes5,episodes6,episodes7,episodes8,episodes9,episodes10" > sd_$OUT_NAME
python parseResults.py gRun tra_1_ .log 5 TEST | grep -P 'SUCCESS\tstd' | sed 's/^SUCCESS\tstd\t//g' | sed 's/ \t/,/g' | sed 's/,$//g' >> sd_$OUT_NAME
