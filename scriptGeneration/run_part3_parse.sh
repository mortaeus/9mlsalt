echo "linear"
echo "gauss"

function wait_qstat {
    LENGTH=`qstat | wc | awk '{print $1}'`
    while [ $LENGTH -ne 0 ];
    do
        echo "Waiting for qstat"
        sleep 1
        LENGTH=`qstat | wc | awk '{print $1}'`
    done
}

wait_qstat

POLY_OUT=poly_part3.res
echo "100,200,300,400,500,600,700,800,900,1000" > mean_$POLY_OUT
echo "100,200,300,400,500,600,700,800,900,1000" > sd_$POLY_OUT
python parseResults.py gRun_poly_  tra_1_ .log 1 TEST | grep -P 'SUCCESS\tavg'
python parseResults.py gRun_poly_ tra_1_ .log 1 TEST | grep -P 'SUCCESS\tavg' | sed 's/^SUCCESS\tavg\t//g' | sed 's/ \t/,/g' | sed 's/ //g' | sed 's/,$//g' >> mean_$POLY_OUT
python parseResults.py gRun_poly_ tra_1_ .log 1 TEST | grep -P 'SUCCESS\tstd' | sed 's/^SUCCESS\tstd\t//g' | sed 's/ \t/,/g' | sed 's/ //g' | sed 's/,$//g' >> sd_$POLY_OUT

GAUSS_OUT=gaus_part3.res
echo "v1,v2,100,200,300,400,500,600,700,800,900,1000" > mean_$GAUSS_OUT
echo "v1,v2,100,200,300,400,500,600,700,800,900,1000" > sd_$GAUSS_OUT
vals=('0.1' '1.0' '10')
for v1 in "${vals[@]}"
do
    for v2 in "${vals[@]}"
    do
        echo -n $v1,$v2, >> mean_$GAUSS_OUT
        python parseResults.py gRun_gauss_${v1}_$v2 tra_1_ .log 1 TEST | grep -P 'SUCCESS\tavg' | sed 's/^SUCCESS\tavg\t//g' | sed 's/\t/,/g' | sed 's/ //g' | sed 's/,$//g' >> mean_$GAUSS_OUT
        echo -n $v1,$v2, >> sd_$GAUSS_OUT
        python parseResults.py gRun_gauss_${v1}_$v2 tra_1_ .log 1 TEST | grep -P 'SUCCESS\tstd' | sed 's/^SUCCESS\tstd\t//g' | sed 's/ \t/,/g' | sed 's/ //g' | sed 's/,$//g' >> sd_$GAUSS_OUT
    done
done
